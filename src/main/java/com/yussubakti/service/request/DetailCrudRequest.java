package com.yussubakti.service.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author yussubakti <yus.subakti@gmail.com>
 * @Since 05/11/20
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DetailCrudRequest implements Serializable {

    private String id;
}
