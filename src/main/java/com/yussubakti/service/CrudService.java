package com.yussubakti.service;

import com.yussubakti.service.response.CrudResponse;
import com.yussubakti.service.request.CrudRequest;
import com.yussubakti.service.request.DetailCrudRequest;

import java.text.ParseException;

/**
 * @Author yussubakti <yus.subakti@gmail.com>
 * @Since 05/11/20
 */

public interface CrudService {

    CrudResponse insert(CrudRequest request) throws ParseException;

    CrudResponse detail(DetailCrudRequest request) throws ParseException;

    Boolean delete(DetailCrudRequest request) throws ParseException;
}
