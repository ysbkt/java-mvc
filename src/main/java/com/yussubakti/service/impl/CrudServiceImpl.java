package com.yussubakti.service.impl;

/**
 * @Author yussubakti <yus.subakti@gmail.com>
 * @Since 05/11/20
 */

import com.yussubakti.entity.Crud;
import com.yussubakti.service.CrudService;
import com.yussubakti.service.request.DetailCrudRequest;
import com.yussubakti.service.response.CrudResponse;
import org.springframework.stereotype.Service;
import com.yussubakti.repository.CrudRepository;
import com.yussubakti.service.request.CrudRequest;

import java.text.ParseException;

@Service("crudService")
public class CrudServiceImpl implements CrudService {

    private CrudRepository repository;

    @Override
    public CrudResponse insert(CrudRequest request) throws ParseException {

        Crud crud = new Crud();
        if (request.getId() != null) {
            Crud save = repository.findById(request.getId());
            if (save.getId() != null) {
                crud.setPlatform(request.getPlatform());
                crud.setNamaPlatform(request.getNamaPlatform());
                crud.setDocType(request.getDocType());
                crud.setTermOfPayment(request.getTermOfPayment());
                repository.save(crud);

                CrudResponse response = new CrudResponse();
                response.setId(crud.getId());
                response.setPlatform(crud.getPlatform());
                response.setNamaPlatform(crud.getNamaPlatform());
                response.setDocType(crud.getTermOfPayment());
                response.setTermOfPayment(crud.getTermOfPayment());
            }
        }

        CrudResponse response = new CrudResponse();
        response.setId(crud.getId());
        response.setPlatform(crud.getPlatform());
        response.setNamaPlatform(crud.getNamaPlatform());
        response.setDocType(crud.getTermOfPayment());
        response.setTermOfPayment(crud.getTermOfPayment());

        return response;
    }

    @Override
    public CrudResponse detail(DetailCrudRequest request) throws ParseException {

        Crud detail = repository.findById(request.getId());
        CrudResponse response = new CrudResponse();
        response.setId(detail.getId());
        response.setPlatform(detail.getPlatform());
        response.setNamaPlatform(detail.getNamaPlatform());
        response.setDocType(detail.getDocType());
        response.setTermOfPayment(detail.getTermOfPayment());

        return response;
    }

    @Override
    public Boolean delete(DetailCrudRequest request) throws ParseException {

        boolean response = false;

        if (request.getId() != null){
            Crud validCrud = repository.findById(request.getId());
            if (validCrud.getId() != null){
                repository.delete(validCrud);
                response = true;
            }
        }

        return response;
    }
}
