package com.yussubakti.entity;

import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author yussubakti <yus.subakti@gmail.com>
 * @Since 05/11/20
 */

@Entity
@Table(name = "yussubakti")
@Data
public class Crud {

    @Id
    @Column(columnDefinition = "idrequestBooking")
    private String id;

    @Column(columnDefinition = "id_platform")
    private String platform;

    @Column(columnDefinition = "nama_platform")
    private String namaPlatform;

    @Column(columnDefinition = "doc_type")
    private String docType;

    @Column(columnDefinition = "term_of_payment")
    private String termOfPayment;


}
