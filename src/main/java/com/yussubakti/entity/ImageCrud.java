package com.yussubakti.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author yussubakti <yus.subakti@gmail.com>
 * @Since 05/11/20
 */

@Entity
@Table(name = "images_yussubakti")
@Data
public class ImageCrud {

    @Id
    @Column(columnDefinition = "idrequestbooking")
    private String id;

    @Column(columnDefinition = "description")
    private String description;
}
