package com.yussubakti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @Author yussubakti <yus.subakti@gmail.com>
 * @Since 05/11/20
 */

@ComponentScan(basePackages = {
    "com.yussubakti.*"
}, lazyInit = true)
@EnableJpaAuditing
@SpringBootApplication
public class CrudApplication {
    public static void main(String[] args) {
        SpringApplication.run(CrudApplication.class, args);
    }
}
