package com.yussubakti.repository;

/**
 * @Author yussubakti <yus.subakti@gmail.com>
 * @Since 05/11/20
 */

import com.yussubakti.entity.Crud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CrudRepository extends JpaRepository {

    Crud findById(String id);
}
